package com.ancient.util.spell.item;

import com.ancient.util.spell.data.Data;

public abstract interface Parameterized
{
  public abstract void setParameter(Data... paramVarArgs);
}
