package com.ancient.util.spell.item.method;

import com.ancient.util.spell.item.SpellItem;

public abstract interface Method
  extends SpellItem
{
  public abstract String getName();
}
